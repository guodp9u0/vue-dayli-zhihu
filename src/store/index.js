import Vue from 'vue';
import Vuex from 'vuex';

import * as actions from './actions';
import * as getters from './getters';
import * as mutations from './mutations';

export default {
    state: {},
    actions,
    getters,
    mutations
}