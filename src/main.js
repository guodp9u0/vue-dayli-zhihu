import Vue from 'vue'

import VueRouter from 'vue-router'
import Vuex from 'vuex'

import App from './App.vue'

import router from './router'
import store from './store';

Vue.use(VueRouter);
const vueRouter = new VueRouter(router);

Vue.use(Vuex);
const vueStore = new Vuex.Store(store);

var vm = new Vue({
    router: vueRouter,
    store: vueStore,
    el: '#app',
    render: h => h(App)
})