const about = r => require.ensure([], () => r(require('../pages/about.vue')), 'about');
const home = r => require.ensure([], () => r(require('../pages/home.vue')), 'home');
const news = r => require.ensure([], () => r(require('../pages/news.vue')), 'news');
const newsList = r => require.ensure([], () => r(require('../pages/newsList.vue')), 'newsList');
const setting = r => require.ensure([], () => r(require('../pages/setting.vue')), 'setting');
const theme = r => require.ensure([], () => r(require('../pages/theme.vue')), 'theme');
const themeList = r => require.ensure([], () => r(require('../pages/themeList.vue')), 'themeList');

export default {
    routes: [{
            path: "/",
            redirect: "/home"
        },
        {
            path: "/about",
            name: "about",
            component: about
        },
        {
            path: "/home",
            name: "home",
            component: home
        },
        {
            path: "/news",
            name: "news",
            component: news
        },
        {
            path: "/newsList",
            name: "newsList",
            component: newsList
        },
        {
            path: "/setting",
            name: "setting",
            component: setting
        },
        {
            path: "/theme",
            name: "theme",
            component: theme
        },
        {
            path: "/themeList",
            name: "themeList",
            component: theme
        }
    ]
};